

(setq load-path (cons (expand-file-name "~/emacs/") load-path))
(setq load-path (cons (expand-file-name "~/emacs/lisp/apel/") load-path))

(load (locate-library "emacs_common") nil t nil)

;;画面の設定
(defun setups-for-my-frames (frame)
        "Set display parameters for the current frame"
         (select-frame frame)
         (when window-system frame
            ;(set-frame-size frame 70 60)
					  (set-background-color "gray10")
            (set-foreground-color "white")
            (set-cursor-color "gray11")
						(set-face-background 'region "blue")
            ;; (set-default-font "M+ 2m-9")
            ;(set-default-font "Bitstream Vera Snas Mono-9")
            ;(set-fontset-font (frame-parameter nil 'font)
            ;                  'japanese-jisx0208
            ;                  '("IPAGothic" . "unicode-bmp"))))
))
      (add-hook 'after-make-frame-functions 'setups-for-my-frames)
      (setups-for-my-frames (selected-frame))

(setq frame-background-mode 'dark)
(frame-update-face-colors (selected-frame))

;; カーソル行のハイライト
(defface hlline-face
  '((((class color)
      (background dark))
     ;;(:background "dark state gray"))
     (:background "gray11"
                  :underline "gray24"))
    (((class color)
      (background light))
     (:background "ForestGreen"
                  :underline nil))
    (t ()))
  "*Face used by hl-line.")
(setq hl-line-face 'hlline-face)
;;(setq hl-line-face 'underline)
(global-hl-line-mode)

;;; [ HOME,END でバッファの先頭，末尾に移動 ]
(global-set-key [home] 'beginning-of-buffer)
(global-set-key [end] 'end-of-buffer)

;; ウィンドウを透明化
(add-to-list 'default-frame-alist '(alpha . (75 70)))
(set-frame-parameter (selected-frame) 'alpha '(75 70))

;;
;; YaTeX
;;
(add-to-list 'load-path "~/.emacs.d/site-lisp/yatex")
(autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)
(setq auto-mode-alist
      (append '(("\\.tex$" . yatex-mode)
                ("\\.ltx$" . yatex-mode)
                ("\\.cls$" . yatex-mode)
                ("\\.sty$" . yatex-mode)
                ("\\.clo$" . yatex-mode)
                ("\\.bbl$" . yatex-mode)) auto-mode-alist))
(setq YaTeX-inhibit-prefix-letter t)
(setq YaTeX-kanji-code nil)
(setq YaTeX-use-LaTeX2e t)
(setq YaTeX-use-AMS-LaTeX t)
(setq YaTeX-dvi2-command-ext-alist
      '(("[agx]dvi\\|PictPrinter" . ".dvi")
        ("gv" . ".ps")
        ("Preview\\|TeXShop\\|TeXworks\\|Skim\\|mupdf\\|xpdf\\|Firefox\\|Adobe" . ".pdf")))
(setq tex-command "/usr/local/texlive/2012/bin/x86_64-darwin/pdfplatex")
;(setq tex-command "/usr/texbin/ptex2pdf -l -ot \"-synctex=1\"")
;(setq tex-command "/usr/texbin/ptex2pdf -l -u -ot \"-synctex=1\"")
;(setq tex-command "/usr/local/bin/pdfplatex")
;(setq tex-command "/usr/local/bin/pdfplatex2")
;(setq tex-command "/usr/local/bin/pdfuplatex")
;(setq tex-command "/usr/local/bin/pdfuplatex2")
;(setq tex-command "/usr/texbin/pdflatex -synctex=1")
;(setq tex-command "/usr/texbin/lualatex -synctex=1")
;(setq tex-command "/usr/texbin/luajitlatex -synctex=1")
;(setq tex-command "/usr/texbin/xelatex -synctex=1")
;(setq tex-command "/usr/texbin/latexmk")
;(setq tex-command "/usr/texbin/latexmk -e '$latex=q/platex -synctex=1/' -e '$bibtex=q/pbibtex/' -e  '$makeindex=q/mendex/' -e '$dvipdf=q/dvipdfmx %O -o %D %S/' -norc -gg -pdfdvi")
;(setq tex-command "/usr/texbin/latexmk -e '$latex=q/platex -synctex=1/' -e '$bibtex=q/pbibtex/' -e '$makeindex=q/mendex/' -e '$dvips=q/dvips %O -z -f %S | convbkmk -g > %D/' -e '$ps2pdf=q/ps2pdf %O %S %D/' -norc -gg -pdfps")
;(setq tex-command "/usr/texbin/latexmk -e '$latex=q/uplatex -synctex=1/' -e '$bibtex=q/upbibtex/' -e '$makeindex=q/mendex/' -e '$dvipdf=q/dvipdfmx %O -o %D %S/' -norc -gg -pdfdvi")
;(setq tex-command "/usr/texbin/latexmk -e '$latex=q/uplatex -synctex=1/' -e '$bibtex=q/upbibtex/' -e '$makeindex=q/mendex/' -e '$dvips=q/dvips %O -z -f %S | convbkmk -u > %D/' -e '$ps2pdf=q/ps2pdf %O %S %D/' -norc -gg -pdfps")
;(setq tex-command "/usr/texbin/latexmk -e '$pdflatex=q/pdflatex -synctex=1/' -e '$bibtex=q/bibtex/' -e '$makeindex=q/makeindex/' -norc -gg -pdf")
;(setq tex-command "/usr/texbin/latexmk -e '$pdflatex=q/lualatex -synctex=1/' -e '$bibtex=q/bibtexu/' -e '$makeindex=q/texindy/' -norc -gg -pdf")
;(setq tex-command "/usr/texbin/latexmk -e '$pdflatex=q/luajitlatex -synctex=1/' -e '$bibtex=q/bibtexu/' -e '$makeindex=q/texindy/' -norc -gg -pdf")
;(setq tex-command "/usr/texbin/latexmk -e '$pdflatex=q/xelatex -synctex=1/' -e '$bibtex=q/bibtexu/' -e '$makeindex=q/texindy/' -norc -gg -xelatex")
(setq bibtex-command (cond ((string-match "uplatex\\|-u" tex-command) "/usr/texbin/upbibtex")
                           ((string-match "platex" tex-command) "/usr/texbin/pbibtex")
                           ((string-match "lualatex\\|luajitlatex\\|xelatex" tex-command) "/usr/texbin/bibtexu")
                           ((string-match "pdflatex\\|latex" tex-command) "/usr/texbin/bibtex")
                           (t "/usr/texbin/pbibtex")))
(setq makeindex-command (cond ((string-match "uplatex\\|-u" tex-command) "/usr/texbin/mendex")
                              ((string-match "platex" tex-command) "/usr/texbin/mendex")
                              ((string-match "lualatex\\|luajitlatex\\|xelatex" tex-command) "/usr/texbin/texindy")
                              ((string-match "pdflatex\\|latex" tex-command) "/usr/texbin/makeindex")
                              (t "/usr/texbin/mendex")))
(setq dvi2-command "/usr/bin/open -a Preview")
;(setq dvi2-command "/usr/bin/open -a Skim")
;(setq dvi2-command "/usr/bin/open -a TeXShop")
;(setq dvi2-command "/usr/bin/open -a TeXworks")
(setq dviprint-command-format "/usr/bin/open -a \"Adobe Reader\" `echo %s | sed -e \"s/\\.[^.]*$/\\.pdf/\"`")

(defun skim-forward-search ()
  (interactive)
  (let* ((ctf (buffer-name))
         (mtf)
         (pf)
         (ln (format "%d" (line-number-at-pos)))
         (cmd "/Applications/Skim.app/Contents/SharedSupport/displayline")
         (args))
    (if (YaTeX-main-file-p)
        (setq mtf (buffer-name))
      (progn
        (if (equal YaTeX-parent-file nil)
            (save-excursion
              (YaTeX-visit-main t)))
        (setq mtf YaTeX-parent-file)))
    (setq pf (concat (car (split-string mtf "\\.")) ".pdf"))
    (setq args (concat ln " " pf " " ctf))
    (message (concat cmd " " args))
    (process-kill-without-query
     (start-process-shell-command "displayline" nil cmd args))))

(add-hook 'yatex-mode-hook
          '(lambda ()
             (define-key YaTeX-mode-map (kbd "C-c s") 'skim-forward-search)))

(add-hook 'yatex-mode-hook
          '(lambda ()
             (auto-fill-mode -1)))

;;
;; RefTeX with YaTeX
;;
;(add-hook 'yatex-mode-hook 'turn-on-reftex)
(add-hook 'yatex-mode-hook
          '(lambda ()
             (reftex-mode 1)
             (define-key reftex-mode-map (concat YaTeX-prefix ">") 'YaTeX-comment-region)
             (define-key reftex-mode-map (concat YaTeX-prefix "<") 'YaTeX-uncomment-region)))

(put 'dired-find-alternate-file 'disabled nil)

;; load-pathに追加
(setq load-path (cons (expand-file-name "~/.emacs.d/_EmacsSettings") load-path))

;;関数名ジャンプのためのショートカット追加
;;gtagsとは->http://uguisu.skr.jp/Windows/gtags.html
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GNU GLOBAL(gtags)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(autoload 'gtags-mode "gtags" "" t)
(setq gtags-mode-hook
      '(lambda ()
         (local-set-key "\M-t" 'gtags-find-tag)
         (local-set-key "\M-r" 'gtags-find-rtag)
         (local-set-key "\M-s" 'gtags-find-symbol)
         (local-set-key "\M-f" 'gtags-find-file)
         (local-set-key "\C-t" 'gtags-pop-stack)
				 ))


;; 編集が終了したらEmacsをアイコン化する
(add-hook 'server-done-hook 'iconify-emacs-when-server-is-done)
;; C-x C-cに割り当てる
;;(global-set-key (kbd "C-x C-c") 'server-edit)
;; M-x exitでEmacsを終了できるようにする
;;(defalias 'exit 'save-buffers-kill-emacs)

;; emacsの終了を変更する
; C-x C-cで Emacs を終了しない。
(global-unset-key "\C-x\C-c")
;(defalias 'quit-emacs 'save-buffers-kill-emacs)
;(global-set-key (kbd "C-x C-c") 'ns-do-hide-emacs) ;emacs休止
(defalias 'exit 'save-buffers-kill-emacs) ;M-x exit で emacs終了

;; elisp
;; まず、install-elisp のコマンドを使える様にします。
(require 'install-elisp)
;; 次に、Elisp ファイルをインストールする場所を指定します。
(setq install-elisp-repository-directory "~/.emacs.d/_EmacsSettings/")

;; 行番号の表示
(require 'linum)
(global-linum-mode)


;; emacsclient を利用するためにサーバ起動
;; サーバが起動していた場合は先に起動していた方を優先
(require 'server)
;(unless (server-running-p) (server-start))
;(when (and (functionp 'server-running-p) (not (server-running-p)))(server-start))
(server-start)

;; 左右の余白を消す
(fringe-mode (cons 0 nil))

;; スクロールバーを消す
(scroll-bar-mode nil)

;; eshell用のPATH受け取り
;; ~/.bash_profileと対応
;; 詳細->http://qiita.com/fnobi/items/8906c8e7759751d32b6b
(load-file (expand-file-name "~/.emacs.d/shellenv.el"))

(dolist (path (reverse (split-string (getenv "PATH") ":")))
  (add-to-list 'exec-path path))

;; set eshell aliases
;; 通常のshellコマンドとして実行する場合には頭に*
;; 引数を付ける場合には末尾に$*
(setq eshell-command-aliases-list
      (append
			 (list
        ;;(list "desk" "cd ~/Desktop")
        ;;(list "swipl" "/opt/local/bin/swipl")
        (list "ls" "*ls $*")
				(list "cd" "cd $*")
				(list "pwd" "pwd")
				(list "ls -a" "ls-a")
				(list "e" "*emacsclient $*")
				(list "make" "*make $*")
				(list "source" "*source $*") ;; 動作確認できず．内部に他の関数がある？
				;; (list "BB" "*~/_Shell/_BigBrother.sh")
				;; .bash_profileに登録しているが，*BBでは正常に動作せず．
				;; eコマンドは正常なので，理由がよくわからない．
				;; (list "ssh" "*ssh $*")
				;; <注記> eshell上ではsshは補完がうまく働かない
				;; 参照->http://qiita.com/fnobi/items/8906c8e7759751d32b6b
				;;
				;;				(list "swipl" "/opt/local/bin/swipl")
				;;        (list "swipl" "/opt/local/bin/swipl")
				)
       'eshell-command-aliases-list))

;; 範囲選択でコピーされるのを禁止する
(setq mouse-drag-copy-region nil)
